# VizGraph

A simple tool for visualization of heterogenous network as a semestral project at school.

The main feature is iterative force layout algorithm [1], which simulates repulsive forces between nodes and attractive forces of edges.

It uses data as specified in [included pdf](https://gitlab.com/haluzpav/vizgraph/blob/master/data/IMiGEr_raw_input_format.pdf).

[Full report.](https://gitlab.com/haluzpav/vizgraph/blob/master/report.pdf)

![](https://gitlab.com/haluzpav/vizgraph/raw/master/screens/forced-degreed.PNG)

Entry point is `src/main/java/jgt/gui/MainWindow.java / main`.

## References

1. Fruchterman, T.M., & Reingold, E.M. (1991). Graph Drawing by Force-directed Placement. Softw., Pract. Exper., 21, 1129-1164.
