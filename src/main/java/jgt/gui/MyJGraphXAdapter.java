package jgt.gui;

import com.mxgraph.model.mxCell;
import jgt.JGraphXAdapter;
import jgt.graph.JEdge;
import jgt.graph.JNode;
import jgt.graph.JVec2D;
import org.jgrapht.Graph;
import org.jgrapht.ListenableGraph;

public class MyJGraphXAdapter extends JGraphXAdapter<JNode, JEdge> {

    public MyJGraphXAdapter(ListenableGraph<JNode, JEdge> graph) {
        super(graph);

        addListener(null, (sender, evt) -> {
            // update pos when user manually drags node
            if (!evt.getName().equals("cellsMoved"))
                return;
            loadPositions((Object[]) evt.getProperties().get("cells"));
        });
    }

    public MyJGraphXAdapter(Graph<JNode, JEdge> graph) {
        super(graph);
    }

    @Override
    public void refresh() {
        getCellToVertexMap().forEach((mxICell, jNode) -> {
            JVec2D pos = jNode.getPos();
            mxICell.getGeometry().setX(pos.x);
            mxICell.getGeometry().setY(pos.y);
        });
        super.refresh();
    }

    private void loadPositions(Object... cells) {
        for (Object o : cells) {
            mxCell cell = (mxCell) o;
            if (!cell.isVertex()) {
                continue;
            }
            JVec2D pos = getCellToVertexMap().get(cell).getPos();
            pos.x = cell.getGeometry().getX();
            pos.y = cell.getGeometry().getY();
        }
    }
}
