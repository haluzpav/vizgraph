package jgt.gui;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.mxgraph.model.mxCell;
import com.mxgraph.model.mxICell;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.view.mxGraph;
import jgt.DetailsBuilder;
import jgt.Utils;
import jgt.graph.JEdge;
import jgt.graph.JNode;
import jgt.graph.JVertex;
import jgt.graph.MyGraph;
import jgt.model.*;
import jgt.simulation.ForceSimulator;
import jgt.simulation.ForceSimulatorA;
import jgt.simulation.RotationSimulator;
import jgt.simulation.Simulator;
import org.apache.commons.lang3.StringUtils;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.stream.Collectors;

public class MainWindow {
    private JPanel root;
    private JSplitPane splitRoot;
    private JScrollPane graphComponent;
    private JPanel sidebarPanel;

    private JPanel buttonsPanel;
    private JButton runButton;
    private JSlider fpsSlider;
    private JLabel fpsValue;

    private JPanel detailsPanel;
    private JTextPane detailsText;
    private JPanel archetypePanel;
    private JTextPane archetypeText;
    private JPanel attributesPanel;
    private JTextPane attributesText;
    private JComboBox<Simulator> simulatorComboBox;
    private JComboBox<InitialPosition> positionsComboBox;
    private JButton setPositionsButton;
    private JSlider coolingSlider;
    private JCheckBox coolingCheckBox;
    private JSlider CSlider;
    private JLabel coolingValue;
    private JLabel CValue;
    private JComboBox<String> datasetComboBox;
    private JSlider minDegreeSlider;
    private JLabel minDegreeValue;

    private Dataset dataset;
    private MyGraph originalGraph, graph;
    private MyJGraphXAdapter adapter;
    private Simulator simulator;
    private JFrame frame;

    private boolean isRunning;

    private static final double C_SCALE = 100;
    private static final List<Simulator> simulators = Arrays.asList(
            new ForceSimulatorA(1, 1),
            new ForceSimulatorA(1, 2),
            new ForceSimulatorA(1, 3),
            new ForceSimulatorA(1, 5),
            new ForceSimulatorA(1, 10),
            new RotationSimulator(),
            new ForceSimulatorA(5, 1),
            new ForceSimulatorA(10, 1),
            new ForceSimulatorA(50, 1),
            new ForceSimulatorA(100, 1)
    );

    static {
        setLookAndFeel();
    }

    public MainWindow() {
        getGraphComponent().addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                if (simulator instanceof ForceSimulator) {
                    ((ForceSimulator) simulator).setBorder(getBorderRectangle());
                }
            }
        });

        minDegreeSlider.addChangeListener(e -> {
            setMinDegree(minDegreeSlider.getValue());
        });
        runButton.addActionListener(e -> {
            switchRunning();
        });
        CSlider.addChangeListener(e -> {
            setC(CSlider.getValue() / C_SCALE);
        });
        coolingCheckBox.addActionListener(e -> {
            enableCooling(coolingCheckBox.isSelected());
        });
        coolingSlider.addChangeListener(e -> {
            setCooling(coolingSlider.getValue());
        });
        fpsSlider.addChangeListener(e -> {
            setFps(fpsSlider.getValue());
        });

        getGraphComponent().getGraphControl().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                mxCell cell = (mxCell) getGraphComponent().getCellAt(e.getX(), e.getY());
                if (e.getButton() == MouseEvent.BUTTON1) {
                    if (cell == null) {
                        showGraphDetails();
                    } else if (cell.isVertex()) {
                        showNodeDetails(adapter.getCellToVertexMap().get(cell));
                    } else if (cell.isEdge()) {
                        showEdgeDetails(adapter.getCellToEdgeMap().get(cell));
                    }
                } else if (e.getButton() == MouseEvent.BUTTON2) {
                    if (cell != null && cell.isVertex()) {
                        tryExpand(adapter.getCellToVertexMap().get(cell));
                    }
                }
            }
        });

        ComboBoxModel<String> datasetsModel = new DefaultComboBoxModel<>(new String[]{
                "debug",
                "raw",
                "histrocal-data",
                "SW-eng-anonymized-demo-graph",
        });
        datasetComboBox.setModel(datasetsModel);
        datasetComboBox.addActionListener(e -> {
            loadDataset((String) datasetsModel.getSelectedItem());
        });

        ComboBoxModel<InitialPosition> positionsModel = new DefaultComboBoxModel<>(InitialPosition.values());
        positionsModel.setSelectedItem(InitialPosition.RANDOM);
        positionsComboBox.setModel(positionsModel);
        setPositionsButton.addActionListener(e -> {
            getGraphComponent().zoomTo(1, false);
            switch ((InitialPosition) positionsModel.getSelectedItem()) {
                case RANDOM:
                    graph.randomizePositions(getBorderRectangle());
                    break;
                case CIRCLE:
                    graph.circularPositions(getBorderRectangle());
                    break;
                default:
                    throw new IllegalStateException();
            }
            adapter.refresh();
        });

        ComboBoxModel<Simulator> simulatorsModel = new DefaultComboBoxModel<>(new Vector<>(simulators));
        simulatorsModel.setSelectedItem(simulators.get(0));
        simulatorComboBox.setModel(simulatorsModel);
        simulatorComboBox.addActionListener(e -> {
            // change simulator
            setRunning(isRunning);
        });
    }

    public void open() {
        frame = new JFrame("Viz");

        frame.setPreferredSize(new Dimension(1500, 1000));
        // frame.setSize(new Dimension(1500, 1000));
        frame.setLocation(100, 50);

        frame.setContentPane(root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

        splitRoot.setDividerLocation(0.75);
        splitRoot.setResizeWeight(1);
        setupTextPane(detailsText);
        setupTextPane(archetypeText);
        setupTextPane(attributesText);

        setMinDegree(0);
        loadDataset((String) datasetComboBox.getModel().getSelectedItem());
    }

    private void initializeGraph() {
        adapter = new MyJGraphXAdapter(graph);
        getGraphComponent().setGraph(adapter);

        removeSimulator();
        setRunning(false);
        setFps(30);
        setC(1);
        enableCooling(false);
        setCooling(150);
        showGraphDetails();

        // to scrollbar appear reliably
        getGraphComponent().setZoomPolicy(mxGraphComponent.ZOOM_POLICY_NONE);

        setupGlobalProperties();
    }

    private void updateGraph() {
        adapter = new MyJGraphXAdapter(graph);
        getGraphComponent().setGraph(adapter);
        replaceSimulator();
        setupGlobalProperties();
    }

    private void removeSimulator() {
        setRunning(false);
        simulator = null;
    }

    private void replaceSimulator() {
        boolean wasRunning = isRunning;
        setRunning(false);
        simulator = null;
        setRunning(wasRunning);
    }

    private void createUIComponents() {
        // can't use adapter at initialization
        graphComponent = new mxGraphComponent(new mxGraph());
    }

    private synchronized void setMinDegree(int minDegree) {
        minDegreeValue.setText(Integer.toString(minDegree));
        if (originalGraph == null) {
            return;
        }

        graph = originalGraph.getMinDegreeGraph(minDegree);
        updateGraph();
        adapter.refresh();
    }

    private void switchRunning() {
        setRunning(!isRunning);
    }

    private void setRunning(boolean isRunning) {
        this.isRunning = isRunning;
        Simulator selectedSimulator = (Simulator) simulatorComboBox.getModel().getSelectedItem();
        if (selectedSimulator != simulator) {
            changeSimulator(selectedSimulator);
        }
        simulator.setRunning(isRunning);
        runButton.setText(isRunning ? "Pause" : "Run");
        // adapter.setCellsMovable(!isRunning);
        // getGraphComponent().setDragEnabled(!isRunning);
        getGraphComponent().setGridVisible(!isRunning);
        getGraphComponent().setPanning(!isRunning);
        frame.repaint();
    }

    private void changeSimulator(Simulator simulator) {
        if (this.simulator != null) {
            this.simulator.setRunning(false); // stop the old one
        }
        this.simulator = simulator;
        simulator.setGraph(graph);
        simulator.setJ2mxAdapter(adapter);
        simulator.setFps(fpsSlider.getValue());
        if (simulator instanceof ForceSimulator) {
            ForceSimulator fSimulator = (ForceSimulator) simulator;
            fSimulator.setBorder(getBorderRectangle());
            fSimulator.setC(CSlider.getValue() / C_SCALE);
            fSimulator.enableCooling(coolingCheckBox.isSelected());
            fSimulator.setCooling(coolingSlider.getValue());
        }
    }

    private Rectangle getBorderRectangle() {
        int scrollPad = 20;
        Rectangle rect = getGraphComponent().getViewportBorderBounds();
        int maxX = 0, maxY = 0;
        for (mxICell cell : adapter.getCellToVertexMap().keySet()) {
            maxX = Math.max(maxX, (int) cell.getGeometry().getWidth());
            maxY = Math.max(maxY, (int) cell.getGeometry().getHeight());
        }
        rect.setSize(rect.width - maxX - scrollPad, rect.height - maxY - scrollPad);
        return rect;
    }

    private void setC(double C) {
        CSlider.setValue((int) (C * C_SCALE));
        CValue.setText(String.format("%.2f", C));
        if (simulator instanceof ForceSimulator) {
            ((ForceSimulator) simulator).setC(C);
        }
    }

    private void enableCooling(boolean enable) {
        coolingCheckBox.setSelected(enable);
        if (simulator instanceof ForceSimulator) {
            ((ForceSimulator) simulator).enableCooling(enable);
        }
    }

    private void setCooling(double cooling) {
        coolingSlider.setValue((int) (cooling));
        coolingValue.setText(String.format("%.0f", cooling));
        if (simulator instanceof ForceSimulator) {
            ((ForceSimulator) simulator).setCooling(cooling);
        }
    }

    private void setFps(double fps) {
        fpsSlider.setValue((int) fps);
        fpsValue.setText(String.format("%.0f", fps));
        simulator.setFps(fps);
    }

    private void setupTextPane(JTextPane pane) {
        pane.setEnabled(false);
        Color color = pane.getBackground();
        pane.setEnabled(true);
        pane.setBackground(color);
    }

    private void tryExpand(JNode node) {
        if (!node.isCollapsed()) {
            return;
        }
        boolean wasRunning = isRunning;
        setRunning(false); // to avoid ConcurrentModificationException
        graph.addNeighbors(originalGraph, node);
        node.setCollapsed(false);
        adapter.refresh();
        setRunning(wasRunning);
    }

    private void showGraphDetails() {
        setBorderTitle(detailsPanel, "Graph details");
        clearDetails();
        setupDetails(detailsPanel, detailsText, new DetailsBuilder()
                .append("source", dataset.getSource())
                .append("total vertex count", dataset.getVertices().size())
                .append("total edge count", dataset.getEdges().size())
                .append("shown vertex count", graph.vertexSet().size())
                .append("shown edge count", graph.edgeSet().size())
                .toString()
        );
    }

    private void showNodeDetails(JNode node) {
        if (node instanceof JVertex) {
            showVertexDetails((JVertex) node);
        }
    }

    private void showVertexDetails(JVertex jVertex) {
        setBorderTitle(detailsPanel, "Vertex details");
        clearDetails();
        Vertex vertex = jVertex.getVertex();

        setupDetails(detailsPanel, detailsText, new DetailsBuilder()
                .append("id", vertex.getId())
                .append("text", vertex.getText())
                .append("name", vertex.getName())
                .toString()
        );
        detailsPanel.setVisible(true);

        VertexArchetype archetype = dataset.getVertexArchetypes().get(vertex.getArchetype());
        setupDetails(archetypePanel, archetypeText, archetypeToString(vertex.getArchetype(), archetype));
        setupDetails(attributesPanel, attributesText, attributesToString(vertex.getAttributes()));

        frame.revalidate();
        frame.repaint();
    }

    private void showEdgeDetails(JEdge jEdge) {
        setBorderTitle(detailsPanel, "Edge details");
        clearDetails();
        Edge edge = jEdge.getEdge();

        setupDetails(detailsPanel, detailsText, new DetailsBuilder()
                .append("id", edge.getId())
                .append("text", edge.getText())
                .append("from", graph.getEdgeSource(jEdge))
                .append("to", graph.getEdgeTarget(jEdge))
                .toString()
        );
        detailsPanel.setVisible(true);

        EdgeArchetype archetype = dataset.getEdgeArchetypes().get(edge.getArchetype());
        setupDetails(archetypePanel, archetypeText, archetypeToString(edge.getArchetype(), archetype));
        setupDetails(attributesPanel, attributesText, attributesToString(edge.getAttributes()));
    }

    private String archetypeToString(int id, Archetype archetype) {
        return new DetailsBuilder()
                .append("id", id)
                .append("name", archetype.getName())
                .append("text", archetype.getText())
                .toString();
    }

    private String attributesToString(Map<Integer, List<String>> attributes) {
        DetailsBuilder db = new DetailsBuilder();
        attributes.forEach((key, values) -> {
            AttributeType type = dataset.getAttributeTypes().get(key);
            String label = type.getName()
                    + (Utils.isEmpty(type.getText()) ? "" : (" (" + type.getText() + ")"));
            String value;
            if (values.size() == 1) {
                value = values.get(0);
            } else {
                value = values.stream().collect(Collectors.joining(", ", "[", "]"));
            }
            db.append(label, value);
        });
        return db.toString();
    }

    private void setupDetails(JPanel panel, JTextPane pane, String text) {
        panel.setVisible(Utils.isNotEmpty(text));
        pane.setText(text);
    }

    private void clearDetails() {
        detailsPanel.setVisible(false);
        archetypePanel.setVisible(false);
        attributesPanel.setVisible(false);
    }

    private void setBorderTitle(JPanel panel, String text) {
        TitledBorder border = (TitledBorder) panel.getBorder();
        border.setTitle(text);
        panel.repaint();
    }

    private mxGraphComponent getGraphComponent() {
        return (mxGraphComponent) graphComponent;
    }

    private static void setLookAndFeel() {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setupGlobalProperties() {
        adapter.setCellsEditable(false);
        adapter.setCellsResizable(false);
        adapter.setCellsDeletable(false);
        adapter.setCellsDisconnectable(false);

        adapter.setAllowDanglingEdges(false);
        adapter.setEdgeLabelsMovable(false);
        adapter.setConnectableEdges(false);
        adapter.setSplitEnabled(false);
        adapter.setGridEnabled(false);

        getGraphComponent().setConnectable(false);
    }

    private void loadDataset(String name) {
        JsonReader jsonReader;
        try {
            jsonReader = new JsonReader(new FileReader("data/" + name + ".json"));
        } catch (FileNotFoundException e) {
            throw new IllegalStateException("working dir must contain data dir with jsons, json missing: " + name, e);
        }
        dataset = new Gson().fromJson(jsonReader, Dataset.class);
        dataset.transform();
        dataset.setSource(name);

        originalGraph = new MyGraph(dataset);
        graph = originalGraph.getMinDegreeGraph(0);
        initializeGraph();
        graph.randomizePositions(getBorderRectangle());
        adapter.refresh();
    }

    public static void main(String[] args) {
        MainWindow window = new MainWindow();
        window.open();
    }

    private enum InitialPosition {
        RANDOM, CIRCLE;

        @Override
        public String toString() {
            return StringUtils.capitalize(super.toString().toLowerCase());
        }
    }
}
