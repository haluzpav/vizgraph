package jgt.graph;

import jgt.model.Dataset;
import jgt.model.Edge;
import jgt.model.Vertex;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultListenableGraph;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

public class MyGraph extends DefaultListenableGraph<JNode, JEdge> {

    private final Dataset dataset;

    private MyGraph(Dataset dataset, boolean hack) {
        super(new DefaultDirectedGraph<>(JEdge.class));
        this.dataset = dataset;
    }

    public MyGraph(Dataset dataset) {
        super(new DefaultDirectedGraph<>(JEdge.class));
        this.dataset = dataset;
        loadDataset();
    }

    private void loadDataset() {
        for (Vertex vertex : dataset.getVertices()) {
            addVertex(new JVertex(vertex));
        }
        for (Edge edge : dataset.getEdges()) {
            JVertex v1 = dataset.getVertex(edge.getFrom()).getJVertex();
            JVertex v2 = dataset.getVertex(edge.getTo()).getJVertex();
            addEdge(v1, v2, new JEdge(edge));
        }
    }

    public MyGraph getMinDegreeGraph(int minDegree) {
        MyGraph newGraph = new MyGraph(dataset, true);
        for (JNode v : vertexSet()) {
            if (degreeOf(v) < minDegree) {
                continue;
            }
            v.setCollapsed(false);
            newGraph.addVertex(v);
        }
        for (JEdge edge : edgeSet()) {
            JNode v = getEdgeSource(edge), u = getEdgeTarget(edge);
            boolean isV = newGraph.vertexSet().contains(v), isU = newGraph.vertexSet().contains(u);
            if (!isV || !isU) {
                if (isV) {
                    v.setCollapsed(true);
                } else if (isU) {
                    u.setCollapsed(true);
                }
                continue;
            }
            newGraph.addEdge(v, u, edge);
        }
        return newGraph;
    }

    @Override
    public JEdge addEdge(JNode sourceVertex, JNode targetVertex) {
        throw new IllegalStateException("don't call this");
    }

    @Override
    public boolean addEdge(JNode sourceVertex, JNode targetVertex, JEdge jEdge) {
        return super.addEdge(sourceVertex, targetVertex, jEdge);
    }

    public void randomizePositions(Rectangle rectangle) {
        Random rnd = new Random();
        for (JNode v : vertexSet()) {
            v.getPos().x = rnd.nextInt(rectangle.width - rectangle.x) + rectangle.x;
            v.getPos().y = rnd.nextInt(rectangle.height - rectangle.y) + rectangle.y;
        }
    }

    public void circularPositions(Rectangle rectangle) {
        double cx, cy, r;
        cx = rectangle.x + rectangle.width / 2;
        cy = rectangle.y + rectangle.height / 2;
        r = Math.min(rectangle.width, rectangle.height) / 2;
        circularPositions(vertexSet(), cx, cy, r);
    }

    private void circularPositions(Collection<JNode> nodes, double cx, double cy, double r) {
        int i = 0, n = nodes.size();
        for (JNode v : nodes) {
            double rads = ((1.0 * i++ / n) % 1) * 2 * Math.PI;
            v.getPos().x = r * Math.cos(rads) + cx;
            v.getPos().y = r * Math.sin(rads) + cy;
        }
    }

    public void addNeighbors(MyGraph originalGraph, JNode node) {
        List<JNode> added = new ArrayList<>();
        for (JEdge edge : originalGraph.edgeSet()) {
            if (edgeSet().contains(edge)) {
                continue;
            }
            JNode v = originalGraph.getEdgeSource(edge), u = originalGraph.getEdgeTarget(edge);
            if (v != node && u != node) {
                continue;
            }
            JNode other = v == node ? u : v;
            addVertex(other);
            added.add(other);
            addEdge(v, u, edge);
            resolveOther(originalGraph, other);
        }
        if (added.isEmpty()) {
            return;
        }
        double r = 30 * Math.pow(added.size(), 0.3);
        circularPositions(added, node.getPos().x, node.getPos().y, r);
    }

    private void resolveOther(MyGraph originalGraph, JNode node) {
        node.setCollapsed(false);
        for (JEdge edge : originalGraph.edgesOf(node)) {
            JNode v = originalGraph.getEdgeSource(edge), u = originalGraph.getEdgeTarget(edge);
            JNode other = v == node ? u : v;
            if (!vertexSet().contains(other)) {
                node.setCollapsed(true);
                continue;
            }
            addEdge(v, u, edge);
            other.setCollapsed(originalGraph.edgesOf(other).size() > edgesOf(other).size());
        }
    }
}
