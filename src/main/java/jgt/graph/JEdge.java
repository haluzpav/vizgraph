package jgt.graph;

import jgt.model.Edge;
import org.jgrapht.graph.DefaultEdge;

public class JEdge extends DefaultEdge {

    private final Edge edge;

    public JEdge(Edge edge) {
        this.edge = edge;
        edge.setJEdge(this);
    }

    public Edge getEdge() {
        return edge;
    }

    @Override
    public String toString() {
        return edge.toString();
    }
}
