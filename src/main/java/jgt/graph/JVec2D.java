package jgt.graph;

import java.io.Serializable;

public class JVec2D implements Cloneable, Serializable {

    public double x, y;

    public JVec2D() {
        this(0, 0);
    }

    public JVec2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public JVec2D plus(JVec2D other) {
        x += other.x;
        y += other.y;
        return this;
    }

    public JVec2D minus(JVec2D other) {
        x -= other.x;
        y -= other.y;
        return this;
    }

    public double euclid(JVec2D other) {
        double xx = x, yy = y;
        xx -= other.x;
        yy -= other.y;
        return Math.sqrt(xx * xx + yy * yy);
    }

    public double euclid() {
        return Math.sqrt(x * x + y * y);
    }

    public JVec2D reset() {
        x = 0;
        y = 0;
        return this;
    }

    public JVec2D times(double z) {
        x *= z;
        y *= z;
        return this;
    }

    @Override
    public JVec2D clone() {
        try {
            return (JVec2D) super.clone();
        } catch (CloneNotSupportedException e) {
            // fuck checked exceptions
            throw new IllegalStateException(e);
        }
    }

    @Override
    public String toString() {
        return String.format("JVec2D(%f, %f)", x, y);
    }
}
