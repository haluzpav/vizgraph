package jgt.graph;

import jgt.model.Vertex;

public class JVertex extends JNode {

    private final Vertex vertex;

    public JVertex(Vertex vertex) {
        this.vertex = vertex;
        vertex.setJVertex(this);
    }

    @Override
    public String toString() {
        return vertex.toString().replace('_', ' ') + (isCollapsed() ? " *" : "");
    }

    public Vertex getVertex() {
        return vertex;
    }
}
