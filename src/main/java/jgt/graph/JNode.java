package jgt.graph;

import java.io.Serializable;

public class JNode implements Serializable {

    private JVec2D pos = new JVec2D();
    private JVec2D disp = new JVec2D();
    private boolean isCollapsed;

    public JVec2D getPos() {
        return pos;
    }

    public void setPos(JVec2D pos) {
        this.pos = pos;
    }

    public JVec2D getDisp() {
        return disp;
    }

    public void setDisp(JVec2D disp) {
        this.disp = disp;
    }

    public boolean isCollapsed() {
        return isCollapsed;
    }

    public void setCollapsed(boolean collapsed) {
        isCollapsed = collapsed;
    }
}
