package jgt.simulation;

import jgt.graph.JNode;
import jgt.graph.JVec2D;

public class RotationSimulator extends Simulator {

    private double prevTimeSecs;

    public RotationSimulator() {
        super();
    }

    @Override
    protected void step(long iStep) {
        double timeSecs = prevTimeSecs + 1 / fps;
        var iRef = new Object() {
            int i = 0;
        };
        int n = graph.vertexSet().size();
        double radius = 300;
        double rotSpeed = .02;
        double padding = 0.0;
        for (JNode jNode : graph.vertexSet()) {
            JVec2D pos = jNode.getPos();
            double rads = ((timeSecs * rotSpeed + 1.0 * iRef.i++ / n) % 1) * 2 * Math.PI;
            pos.x = radius * (1 + padding + Math.cos(rads));
            pos.y = radius * (1 + padding + Math.sin(rads));
        }
        prevTimeSecs = timeSecs;
    }

}
