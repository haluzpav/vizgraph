package jgt.simulation;

public class ForceSimulatorA extends ForceSimulator {

    public ForceSimulatorA(double tempInitial, double speed) {
        super(tempInitial, speed);
    }

    @Override
    protected double fa(double z) {
        return z * z / k;
    }

    @Override
    protected double fr(double z) {
        return k * k / z;
    }

    @Override
    protected double cool(double t) {
        // return tempInitial / Math.pow(iStepCooled, 0.1);
        return tempInitial * Math.exp(-iStepCooled / tempDecay);
    }

}
