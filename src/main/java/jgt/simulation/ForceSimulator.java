package jgt.simulation;

import jgt.graph.JEdge;
import jgt.graph.JNode;
import jgt.graph.JVec2D;

import java.awt.*;
import java.util.Random;

public abstract class ForceSimulator extends Simulator {

    double C;
    final double speed;

    boolean cooling;
    final double tempInitial;
    double tempDecay;
    int iStepCooled;

    double n;
    double area;
    double k;
    double temp;

    private Rectangle border;
    private double borderRepulsion = 0.2;

    private static double zeroThr = 1e-3;
    private static Random rnd = new Random();

    ForceSimulator(double tempInitial, double speed) {
        super();
        this.tempInitial = tempInitial;
        temp = tempInitial;
        this.speed = speed;
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();

        n = graph.vertexSet().size();
        borderRepulsion = Math.pow(n, 1.7) / 70; // #shrug
        resetK();
        temp = tempInitial;
    }

    private void resetK() {
        area = border.width * border.height;
        k = C * Math.sqrt(area / n);
    }

    @Override
    protected void step(long iStep) {
        // repulsion of nodes
        for (JNode v : graph.vertexSet()) {
            v.getDisp().reset();
            for (JNode u : graph.vertexSet()) {
                if (u == v) {
                    continue;
                }
                JVec2D diff = v.getPos().clone().minus(u.getPos());
                double diffAbs = assureNonZero(diff.euclid());
                diff.times(fr(diffAbs) / diffAbs);
                v.getDisp().plus(diff);
            }
        }

        // repulsion of borders
        for (JNode v : graph.vertexSet()) {
            for (int b = 0; b < 4; b++) {
                JVec2D diff = getBorderDiff(v.getPos(), b);
                double diffAbs = assureNonZero(diff.euclid());
                diff.times(fr(diffAbs) / diffAbs * borderRepulsion);
                v.getDisp().plus(diff);
            }
        }

        // attraction of edges
        for (JEdge jEdge : graph.edgeSet()) {
            JNode v = graph.getEdgeSource(jEdge);
            JNode u = graph.getEdgeTarget(jEdge);
            JVec2D diff = v.getPos().clone().minus(u.getPos());
            double diffAbs = assureNonZero(diff.euclid());
            diff.times(fa(diffAbs) / diffAbs);
            v.getDisp().minus(diff);
            u.getDisp().plus(diff);
        }

        // update pos (limit disp by temp)
        for (JNode v : graph.vertexSet()) {
            double dispAbs = assureNonZero(v.getDisp().euclid());
            double dispLimit = Math.min(dispAbs, temp);
            v.getDisp().times(speed * dispLimit / dispAbs);
            v.getPos().plus(v.getDisp());

            v.getPos().x = Math.min(border.getMaxX(), Math.max(border.x, v.getPos().x));
            v.getPos().y = Math.min(border.getMaxY(), Math.max(border.y, v.getPos().y));
        }

        if (cooling) {
            temp = cool(temp);
            iStepCooled++;
        }
    }

    private JVec2D getBorderDiff(JVec2D vPos, int b) {
        switch (b) {
            case 0:
                return new JVec2D(border.x, vPos.y);
            case 1:
                return new JVec2D(border.x, vPos.y - border.getMaxY());
            case 2:
                return new JVec2D(vPos.x, border.y);
            case 3:
                return new JVec2D(vPos.x - border.getMaxX(), border.y);
            default:
                throw new IllegalArgumentException();
        }
    }

    protected abstract double fa(double z);

    protected abstract double fr(double z);

    protected abstract double cool(double t);

    private static double assureNonZero(double z) {
        while (Math.abs(z) < zeroThr) {
            z += rnd.nextDouble() * zeroThr * 10;
        }
        return z;
    }

    @Override
    public String toString() {
        return String.format("%s(temp=%.3f, speed=%.3f)", getClass().getSimpleName(), temp, speed);
    }

    public void setBorder(Rectangle border) {
        this.border = border;
        resetK();
    }

    public void setC(double C) {
        this.C = C;
        resetK();
    }

    public void enableCooling(boolean enable) {
        cooling = enable;
        temp = tempInitial;
        iStepCooled = 0;
    }

    public void setCooling(double cooling) {
        tempDecay = cooling;
    }
}
