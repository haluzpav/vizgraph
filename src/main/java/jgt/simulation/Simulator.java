package jgt.simulation;

import jgt.graph.MyGraph;
import jgt.gui.MyJGraphXAdapter;

public abstract class Simulator {

    MyGraph graph;
    private MyJGraphXAdapter j2mxAdapter;

    private boolean wasInitialized;
    private boolean isRunning;
    private Thread thread;
    long iStep;

    double fps = 10;

    Simulator() {
    }

    public void setRunning(boolean isRunning) {
        this.isRunning = isRunning;
        if (thread == null || !thread.isAlive()) {
            start();
        }
    }

    private void start() {
        if (!wasInitialized) {
            // user can pause
            onInitialize();
            wasInitialized = true;
        }
        thread = new Thread(() -> {
            onResume();
            while (isRunning) {
                step(iStep++);
                j2mxAdapter.refresh();
                try {
                    Thread.sleep((long) (1_000.0 / fps));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.setDaemon(true);
        thread.start();
    }

    /**
     * Before first step.
     */
    protected void onInitialize() {

    }

    /**
     * On un-pausing or after initialization.
     */
    protected void onResume() {

    }

    abstract protected void step(long iStep);

    public void setGraph(MyGraph graph) {
        this.graph = graph;
    }

    public void setJ2mxAdapter(MyJGraphXAdapter j2mxAdapter) {
        this.j2mxAdapter = j2mxAdapter;
    }

    public void setFps(double fps) {
        this.fps = fps;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName();
    }
}
