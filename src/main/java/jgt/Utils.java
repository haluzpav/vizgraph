package jgt;

import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.List;

public final class Utils {

    private Utils() {
    }

    private static final List<String> EMPTY = Arrays.asList(
            "null",
            "not used",
            "unknown"
    );

    public static String getFirstNonEmpty(String... strings) {
        for (String string : strings) {
            if (isNotEmpty(string)) {
                return string;
            }
        }
        return null;
    }

    public static String getFirstNonEmpty(List<String> strings) {
        for (String string : strings) {
            if (isNotEmpty(string)) {
                return string;
            }
        }
        return null;
    }

    public static boolean isEmpty(String s) {
        return StringUtils.isEmpty(s) || EMPTY.contains(s);
    }

    public static boolean isNotEmpty(String s) {
        return !isEmpty(s);
    }

}
