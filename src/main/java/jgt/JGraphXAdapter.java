package jgt;

import com.mxgraph.model.mxICell;
import com.mxgraph.view.mxGraph;
import org.jgrapht.Graph;
import org.jgrapht.ListenableGraph;
import org.jgrapht.event.GraphEdgeChangeEvent;
import org.jgrapht.event.GraphListener;
import org.jgrapht.event.GraphVertexChangeEvent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

public class JGraphXAdapter<V, E>
        extends mxGraph
        implements GraphListener<V, E> {
    private final Graph<V, E> graphT;
    private final HashMap<V, mxICell> vertexToCellMap = new HashMap<>();
    private final HashMap<E, mxICell> edgeToCellMap = new HashMap<>();
    private final HashMap<mxICell, V> cellToVertexMap = new HashMap<>();
    private final HashMap<mxICell, E> cellToEdgeMap = new HashMap<>();

    public JGraphXAdapter(ListenableGraph<V, E> graph) {
        this((Graph<V, E>) graph);
        graph.addGraphListener(this);
    }

    public JGraphXAdapter(Graph<V, E> graph) {
        super();
        this.graphT = Objects.requireNonNull(graph);
        insertJGraphT(graph);
        setAutoSizeCells(true);
    }

    public HashMap<V, mxICell> getVertexToCellMap() {
        return vertexToCellMap;
    }

    public HashMap<E, mxICell> getEdgeToCellMap() {
        return edgeToCellMap;
    }

    public HashMap<mxICell, E> getCellToEdgeMap() {
        return cellToEdgeMap;
    }

    public HashMap<mxICell, V> getCellToVertexMap() {
        return cellToVertexMap;
    }

    @Override
    public void vertexAdded(GraphVertexChangeEvent<V> e) {
        addJGraphTVertex(e.getVertex());
    }

    @Override
    public void vertexRemoved(GraphVertexChangeEvent<V> e) {
        mxICell cell = vertexToCellMap.remove(e.getVertex());
        removeCells(new Object[]{cell});

        cellToVertexMap.remove(cell);
        vertexToCellMap.remove(e.getVertex());

        ArrayList<E> removedEdges = new ArrayList<>();

        for (E edge : cellToEdgeMap.values()) {
            if (!graphT.edgeSet().contains(edge)) {
                removedEdges.add(edge);
            }
        }

        for (E edge : removedEdges) {
            removeEdge(edge);
        }
    }

    @Override
    public void edgeAdded(GraphEdgeChangeEvent<V, E> e) {
        addJGraphTEdge(e.getEdge());
    }

    @Override
    public void edgeRemoved(GraphEdgeChangeEvent<V, E> e) {
        removeEdge(e.getEdge());
    }

    private void removeEdge(E edge) {
        mxICell cell = edgeToCellMap.remove(edge);
        removeCells(new Object[]{cell});

        cellToEdgeMap.remove(cell);
        edgeToCellMap.remove(edge);
    }

    private void addJGraphTVertex(V vertex) {
        getModel().beginUpdate();

        try {
            mxICell cell = (mxICell) insertVertex(defaultParent, null, vertex, 0, 0, 0, 0);

            updateCellSize(cell);

            vertexToCellMap.put(vertex, cell);
            cellToVertexMap.put(cell, vertex);
        } finally {
            getModel().endUpdate();
        }
    }

    private void addJGraphTEdge(E edge) {
        getModel().beginUpdate();

        try {
            V sourceVertex = graphT.getEdgeSource(edge);
            V targetVertex = graphT.getEdgeTarget(edge);

            if (!(vertexToCellMap.containsKey(sourceVertex) && vertexToCellMap.containsKey(targetVertex))) {
                return;
            }

            Object sourceCell = vertexToCellMap.get(sourceVertex);
            Object targetCell = vertexToCellMap.get(targetVertex);

            mxICell cell = (mxICell) insertEdge(defaultParent, null, edge, sourceCell, targetCell);

            updateCellSize(cell);

            edgeToCellMap.put(edge, cell);
            cellToEdgeMap.put(cell, edge);
        } finally {
            getModel().endUpdate();
        }
    }

    private void insertJGraphT(Graph<V, E> graph) {
        for (V vertex : graph.vertexSet()) {
            addJGraphTVertex(vertex);
        }

        for (E edge : graph.edgeSet()) {
            addJGraphTEdge(edge);
        }
    }
}
