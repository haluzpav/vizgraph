package jgt.model;

public abstract class Archetype {

    private String name;
    private String text;

    public String getName() {
        return name;
    }

    public String getText() {
        return text;
    }

}
