package jgt.model;


public class SideBar {

    private String id;
    private boolean isIconsDisplayed;

    public String getId() {
        return id;
    }

    public boolean isIsIconsDisplayed() {
        return isIconsDisplayed;
    }

}
