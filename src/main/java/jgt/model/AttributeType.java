package jgt.model;


public class AttributeType {

    private String dataType;
    private String name;
    private String text;

    public String getDataType() {
        return dataType;
    }

    public String getName() {
        return name;
    }

    public String getText() {
        return text;
    }

}
