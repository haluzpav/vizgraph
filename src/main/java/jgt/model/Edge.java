package jgt.model;

import com.google.gson.internal.LinkedTreeMap;
import jgt.graph.JEdge;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class Edge implements Serializable {

    private List<SubedgeInfo> subedgeInfo = new ArrayList<>();
    private int from;
    private int to;
    private String text;
    private int id;
    private Integer archetype;

    private Map<Integer, Object> attributes = new LinkedTreeMap<>();

    private transient JEdge jEdge;
    private transient boolean _transformedAttrs;

    /**
     * Used only in "raw.json" and only for minority of edges. Will not use.
     */
    private List<SubedgeInfo> getSubedgeInfo() {
        return subedgeInfo;
    }

    public int getFrom() {
        return from;
    }

    public int getTo() {
        return to;
    }

    public String getText() {
        return text;
    }

    public int getId() {
        return id;
    }

    public int getArchetype() {
        return archetype;
    }

    @SuppressWarnings("unchecked")
    public Map<Integer, List<String>> getAttributes() {
        if (!_transformedAttrs) {
            throw new IllegalStateException("TRANSFOOOORM");
        }
        return (Map<Integer, List<String>>) (Object) attributes;
    }

    public JEdge getJEdge() {
        return jEdge;
    }

    public void setJEdge(JEdge jEdge) {
        this.jEdge = jEdge;
    }

    @Override
    public String toString() {
        // TODO
        // return Utils.getFirstNonEmpty(text, Integer.toString(id));
        return "";
    }

    @SuppressWarnings("unchecked")
    void transform(Dataset dataset) {
        if (archetype == null && subedgeInfo.size() > 0) {
            SubedgeInfo si = subedgeInfo.get(0);
            archetype = si.getArchetype();
            for (List<String> attribute : si.getAttributes()) {
                if (attribute.size() < 2) {
                    throw new IllegalStateException();
                }
                int key = dataset.findAttributeType(attribute.get(0));
                attributes.put(key, attribute.subList(1, attribute.size()));
            }
        } else {
            Map<Integer, List<String>> newAttrs = new LinkedTreeMap<>();
            attributes.forEach((integer, o) -> {
                if (o instanceof String) {
                    newAttrs.put(integer, Collections.singletonList((String) o));
                } else if (o instanceof List) {
                    newAttrs.put(integer, (List<String>) o);
                } else {
                    throw new IllegalStateException();
                }
            });
            attributes = (Map<Integer, Object>) (Object) newAttrs;
        }
        _transformedAttrs = true;
    }
}
