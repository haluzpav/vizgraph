package jgt.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SubedgeInfo implements Serializable {

    private int archetype;
    private List<List<String>> attributes = new ArrayList<>();
    private int id;

    public int getArchetype() {
        return archetype;
    }

    public List<List<String>> getAttributes() {
        return attributes;
    }

    public int getId() {
        return id;
    }

}
