package jgt.model;

import com.google.gson.internal.LinkedTreeMap;
import jgt.Utils;
import jgt.graph.JVertex;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class Vertex implements Serializable {

    private int archetype;
    /**
     * By default either one of
     * [["key1", "Value1"], ["key1", "Value1"], ...]
     * {"2": "asd", "5": ["ddsad", "asdw", ...], ...}
     */
    private Object attributes;
    private int id;
    private String text;
    private String name;
    private Position position;

    private transient JVertex jVertex;
    private transient String _string;
    private boolean _transformedAttrs;

    public int getArchetype() {
        return archetype;
    }

    @SuppressWarnings("unchecked")
    public Map<Integer, List<String>> getAttributes() {
        if (!_transformedAttrs) {
            throw new IllegalStateException("TRANSFOOOORM");
        }
        return (Map<Integer, List<String>>) attributes;
    }

    public int getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public String getName() {
        return name;
    }

    public Position getPosition() {
        return position;
    }

    public JVertex getJVertex() {
        return jVertex;
    }

    public void setJVertex(JVertex jVertex) {
        this.jVertex = jVertex;
    }

    private String findBestString() {
        String s;
        s = Utils.getFirstNonEmpty(name, text);
        if (s != null) {
            return s;
        }
        s = getAttributes().values().stream()
                .map(Utils::getFirstNonEmpty)
                .filter(Objects::nonNull)
                .findFirst()
                .orElse(null);
        if (s != null) {
            return s;
        }
        return Integer.toString(id);
    }

    @Override
    public String toString() {
        return _string == null ? (_string = StringUtils.abbreviate(findBestString(), 10)) : _string;
    }

    @SuppressWarnings("unchecked")
    void transformAttributes(Dataset dataset) {
        Map<Integer, List<String>> newAttrs = new LinkedTreeMap<>();
        if (attributes instanceof List) {
            for (List<String> attribute : (List<List<String>>) attributes) {
                int key = dataset.findAttributeType(attribute.get(0));
                newAttrs.put(key, Collections.singletonList(attribute.get(1)));
            }
        } else if (attributes instanceof Map) {
            ((Map<String, Object>) attributes).forEach((s, o) -> {
                int key = Integer.parseInt(s);
                if (o instanceof List) {
                    newAttrs.put(key, (List<String>) o);
                } else if (o instanceof String) {
                    newAttrs.put(key, Collections.singletonList((String) o));
                } else {
                    throw new IllegalStateException("not implemented");
                }
            });
        }
        attributes = newAttrs;
        _transformedAttrs = true;
    }
}
