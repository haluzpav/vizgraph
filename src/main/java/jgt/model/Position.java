package jgt.model;


import java.io.Serializable;

public class Position implements Serializable {

    private double x;
    private double y;

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

}
