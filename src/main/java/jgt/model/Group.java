package jgt.model;

import java.util.ArrayList;
import java.util.List;

public class Group {

    private int id;
    private String name;
    private List<Integer> verticesId = new ArrayList<>();
    private List<Integer> verticesEdgeFromId = new ArrayList<>();
    private List<Integer> verticesEdgeToId = new ArrayList<>();
    private Position position;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<Integer> getVerticesId() {
        return verticesId;
    }

    public List<Integer> getVerticesEdgeFromId() {
        return verticesEdgeFromId;
    }

    public List<Integer> getVerticesEdgeToId() {
        return verticesEdgeToId;
    }

    public Position getPosition() {
        return position;
    }

}
