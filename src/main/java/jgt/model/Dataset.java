
package jgt.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Dataset {

    private List<AttributeType> attributeTypes = new ArrayList<>();
    private List<EdgeArchetype> edgeArchetypes = new ArrayList<>();
    private List<VertexArchetype> vertexArchetypes = new ArrayList<>();
    private List<Vertex> vertices = new ArrayList<>();
    private List<Edge> edges = new ArrayList<>();
    private Map<Integer, List<String>> possibleEnumValues = new HashMap<>();
    private List<Group> groups = new ArrayList<>();
    private List<SideBar> sideBar = new ArrayList<>();
    private String highlightedVertex;
    private String highlightedEdge;

    private transient String source;

    public List<AttributeType> getAttributeTypes() {
        return attributeTypes;
    }

    public List<EdgeArchetype> getEdgeArchetypes() {
        return edgeArchetypes;
    }

    public List<VertexArchetype> getVertexArchetypes() {
        return vertexArchetypes;
    }

    public List<Vertex> getVertices() {
        return vertices;
    }

    public List<Edge> getEdges() {
        return edges;
    }

    public Map<Integer, List<String>> getPossibleEnumValues() {
        return possibleEnumValues;
    }

    public List<Group> getGroups() {
        return groups;
    }

    public List<SideBar> getSideBar() {
        return sideBar;
    }

    public String getHighlightedVertex() {
        return highlightedVertex;
    }

    public String getHighlightedEdge() {
        return highlightedEdge;
    }

    public Vertex getVertex(int id) {
        for (Vertex vertex : getVertices()) {
            if (vertex.getId() == id) {
                return vertex;
            }
        }
        throw new IllegalArgumentException();
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public void transform() {
        for (Vertex vertex : getVertices()) {
            vertex.transformAttributes(this);
        }
        for (Edge edge : getEdges()) {
            edge.transform(this);
        }
    }

    int findAttributeType(String s) {
        int found = -1;
        for (int i = 0; i < attributeTypes.size(); i++) {
            AttributeType type = attributeTypes.get(i);
            if (type.getName().equals(s)) {
                found = i;
                break;
            }
        }
        return found;
    }
}
