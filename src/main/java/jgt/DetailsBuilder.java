package jgt;

import org.apache.commons.lang3.StringUtils;

public class DetailsBuilder {

    private StringBuilder sb = new StringBuilder();

    public DetailsBuilder append(String label, String value) {
        if (Utils.isEmpty(value)) {
            return this;
        }
        if (sb.length() > 0) {
            sb.append("\n");
        }
        sb.append(StringUtils.capitalize(label)).append(": ").append(StringUtils.abbreviate(value, 200));
        return this;
    }

    public DetailsBuilder append(String label, Object object) {
        return append(label, object.toString());
    }

    @Override
    public String toString() {
        return sb.toString();
    }
}
